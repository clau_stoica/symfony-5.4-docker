setup:

- create "mysql" folder

- docker-compose up -d --build

- docker exec -it php bash

- symfony new .

- (optional)
	- yarn install
	- yarn encore dev --watch


useful urls:

- https://www.twilio.com/blog/get-started-docker-symfony

- https://gist.github.com/michaelneu/2ca7987ef00fa3fbe4fd7b9c07834cc7

- https://stackoverflow.com/questions/52565221/docker-supervisor-symfony-command

- http://www.inanzzz.com/index.php/post/6tik/using-supervisor-withing-docker-containers

- https://symfony.com/doc/5.4/messenger.html

- https://stackoverflow.com/questions/44447821/how-to-create-a-docker-image-for-php-and-node